use crossterm::{
    cursor::{self, position},
    event::{poll, read, Event, KeyCode},
    style::{self, Stylize},
    terminal, ExecutableCommand, QueueableCommand, Result,
};
use std::io;
use std::{
    io::{stdout, Stdout, Write},
    str::FromStr,
};
use tictactoers::{Game, Position};
// >18 ^10
//   A     B     C
//      |     |
//1  X  |  -  |  -
// _____|_____|_____
//      |     |
//2  -  |  O  |  X
// _____|_____|_____
//      |     |
//3  -  |  O  |  -
//      |     |

struct GameUI {
    original_size: (u16, u16),
    player1_name: String,
    player2_name: String,
    game: Game,
    stdout: Stdout,
}

impl GameUI {
    pub fn new(stdout: Stdout) -> Self {
        GameUI {
            original_size: terminal::size().unwrap(),
            player1_name: String::from("Player1"),
            player2_name: String::from("Player2"),
            stdout,
            game: Game::new(),
        }
    }
    pub fn run(&mut self) {
        self.prepare_ui();
        self.render();

        loop {
            let key_event = read().unwrap();
            match key_event {
                Event::Key(key) => match key.code {
                    KeyCode::Enter => self.game.player1.set(Position::A1),
                    KeyCode::Char('q') => break,
                    _ => (),
                },
                _ => (),
            }
            self.render();
        }

        self.restore_ui();
        println!("Game Over!");
    }

    fn render(&mut self) {
        let grid1 = format!("  {}  |  {}  |  {}", "X", "O", "X");
        let grid2 = "  -  |  -  |  -";
        let grid3 = "  -  |  -  |  -";
        self.stdout
            .queue(cursor::MoveTo(0, 0))
            .unwrap()
            .queue(style::PrintStyledContent("   A     B     C".magenta()))
            .unwrap()
            .queue(cursor::MoveTo(0, 1))
            .unwrap()
            .queue(style::Print("      |     |"))
            .unwrap()
            .queue(cursor::MoveTo(0, 2))
            .unwrap()
            .queue(style::PrintStyledContent("1".magenta()))
            .unwrap()
            .queue(cursor::MoveTo(1, 2))
            .unwrap()
            .queue(style::Print(grid1))
            .unwrap()
            .queue(cursor::MoveTo(0, 3))
            .unwrap()
            .queue(style::Print(" _____|_____|_____"))
            .unwrap()
            .queue(cursor::MoveTo(0, 4))
            .unwrap()
            .queue(style::Print("      |     |"))
            .unwrap()
            .queue(cursor::MoveTo(0, 5))
            .unwrap()
            .queue(style::PrintStyledContent("2".magenta()))
            .unwrap()
            .queue(cursor::MoveTo(1, 5))
            .unwrap()
            .queue(style::Print(grid2))
            .unwrap()
            .queue(cursor::MoveTo(0, 6))
            .unwrap()
            .queue(style::Print(" _____|_____|_____"))
            .unwrap()
            .queue(cursor::MoveTo(0, 7))
            .unwrap()
            .queue(style::Print("      |     |"))
            .unwrap()
            .queue(cursor::MoveTo(0, 8))
            .unwrap()
            .queue(style::PrintStyledContent("3".magenta()))
            .unwrap()
            .queue(cursor::MoveTo(1, 8))
            .unwrap()
            .queue(style::Print(grid3))
            .unwrap()
            .queue(cursor::MoveTo(0, 9))
            .unwrap()
            .queue(style::Print("      |     |     "))
            .unwrap()
            .queue(cursor::MoveTo(3, 2))
            .unwrap();
    }

    fn prepare_ui(&mut self) {
        terminal::enable_raw_mode().unwrap();
        self.stdout
            .execute(terminal::Clear(terminal::ClearType::All))
            .unwrap()
            .execute(cursor::Hide)
            .unwrap()
            .execute(cursor::MoveTo(0, 0))
            .unwrap();
    }

    fn restore_ui(&mut self) {
        let (cols, rows) = self.original_size;
        self.stdout
            .execute(terminal::SetSize(cols, rows))
            .unwrap()
            .execute(terminal::Clear(terminal::ClearType::All))
            .unwrap()
            .execute(cursor::Show)
            .unwrap();
        terminal::disable_raw_mode().unwrap();
    }
}

struct GameConsole {
    view: Vec<String>,
}

impl GameConsole {
    fn new() -> Self {
        GameConsole {
            view: vec![String::from("-"); 9],
        }
    }
    fn print(&self) {
        for v in self.view.chunks(3) {
            println!("{}", v.join("|"));
        }
    }
    fn run(&mut self) {
        let mut game = Game::new();
        let mut cur_player = &mut game.player1;
        loop {
            self.print();
            let pos = input_position();
            match pos {
                Position::A1 => self.view[0] = String::from("X"),
                Position::A2 => self.view[1] = String::from("X"),
                Position::A3 => self.view[2] = String::from("X"),
                Position::B1 => self.view[3] = String::from("X"),
                Position::B2 => self.view[4] = String::from("X"),
                Position::B3 => self.view[5] = String::from("X"),
                Position::C1 => self.view[6] = String::from("X"),
                Position::C2 => self.view[7] = String::from("X"),
                Position::C3 => self.view[8] = String::from("X"),
            }
            cur_player.set(pos);
            if cur_player.check_win() {
                println!("Won!");
            }
        }
    }
}

fn input(prompt: &str) -> String {
    print!("{}", prompt);
    let mut input = String::new();
    stdout().flush().expect("Failed to flush stdout!");
    io::stdin()
        .read_line(&mut input)
        .expect("Failed to read line");
    input.pop();
    return input;
}
fn input_position() -> Position {
    loop {
        let userin = input("input Position: ");
        match Position::from_str(&userin) {
            Ok(pos) => return pos,
            Err(_) => println!("Invalid position {userin}"),
        }
    }
}

fn main() {
    let mut ui = GameConsole::new();
    ui.run();
}
