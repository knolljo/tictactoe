use std::str::FromStr;

#[repr(u32)]
#[derive(Debug)]
pub enum Position {
    A1 = 0x80080080,
    A2 = 0x40008000,
    A3 = 0x20000808,
    B1 = 0x08040000,
    B2 = 0x04004044,
    B3 = 0x02000400,
    C1 = 0x00820002,
    C2 = 0x00402000,
    C3 = 0x00200220,
}

impl FromStr for Position {
    type Err = &'static str;

    fn from_str(pos_str: &str) -> Result<Position, Self::Err> {
        match pos_str {
            "a1" => Ok(Position::A1),
            "a2" => Ok(Position::A2),
            "a3" => Ok(Position::A3),
            "b1" => Ok(Position::B1),
            "b2" => Ok(Position::B2),
            "b3" => Ok(Position::B3),
            "c1" => Ok(Position::C1),
            "c2" => Ok(Position::C2),
            "c3" => Ok(Position::C3),
            _ => Err("not a valid position"),
        }
    }
}

pub struct Player(u32);
impl Player {
    pub fn set(&mut self, pos: Position) {
        self.0 |= pos as u32;
    }
    pub fn check_win(&self) -> bool {
        let x = self.0 & (self.0 << 1) & (self.0 >> 1);
        x != 0
    }
}

pub struct Game {
    pub player1: Player,
    pub player2: Player,
}

impl Game {
    pub fn new() -> Self {
        Game {
            player1: Player(0),
            player2: Player(0),
        }
    }
}

// fn main() {
//     let mut game = Game::new();
//     game.board1.set(Position::A1);
// }

// enum WinPattern {
//     RowTop,
//     RowMiddle,
//     RowBottom,
//     ColLeft,
//     ColMiddle,
//     ColRight,
//     DiagRight,
//     DiagLeft,
// }

// fn get_win_pattern(board: &u32) -> Option<WinPattern> {
//     let index = (board.leading_zeros() - 2) / 4;
//     match index {
//         0 => Some(WinPattern::RowTop),
//         1 => Some(WinPattern::RowMiddle),
//         2 => Some(WinPattern::RowBottom),
//         3 => Some(WinPattern::RowBottom),
//         4 => Some(WinPattern::ColLeft),
//         5 => Some(WinPattern::ColMiddle),
//         6 => Some(WinPattern::ColRight),
//         8 => Some(WinPattern::DiagLeft),
//         9 => Some(WinPattern::DiagRight),
//         _ => None,
//     }
// }
